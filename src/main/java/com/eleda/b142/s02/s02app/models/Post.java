package com.eleda.b142.s02.s02app.models;

import javax.persistence.*;

@Entity
@Table(name="posts")
public class Post {
    // Properties (columns)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // primary key
    @Column
    private String title;
    @Column
    private String content;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // Constructors
    // Data: title, content

    // Empty
    public Post() {}

    // Parameterized
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    // Getters & Setters
    //Getters
    public String getTitle() {

        return title;
    }

    public String getContent() {

        return content;
    }

    public User getUser() {
        return user;
    }
    //Setters
    public void setTitle(String newTitle) {

        this.title = newTitle;
    }

    public void setContent(String newContent) {

        this.content = newContent;
    }

    public void setUser(User author) {
        this.user = author;
    }
    // Methods
}
