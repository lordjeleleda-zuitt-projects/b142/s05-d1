package com.eleda.b142.s02.s02app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {
    // Properties (columns)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // primary key
    @Column
    private String username;
    @Column
    private String password;
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<Post> posts;

    // Constructors
    // Data: title, content

    // Empty
    public User() {}

    // Parameterized
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // Getters & Setters
    //Getters
    public String getUsername() {

        return username;
    }

    public String getPassword() {

        return password;
    }

    public Set<Post> getPosts () {
        return posts;
    }

    public Long getId() {
        return id;
    }

    //Setters
    public void setUsername(String newUsername) {

        this.username = newUsername;
    }

    public void setPassword(String newPassword) {

        this.password = newPassword;
    }
    // Methods
}
